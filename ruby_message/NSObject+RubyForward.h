//
//  NSObject+RubyForward.h
//  ruby_message
//
//  Created by Graham Lee on 30/11/2013.
//  Copyright (c) 2013 Graham Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (GJLRubyForward)

- (id)methodMissing:(SEL)aSelector :(NSArray *)arguments;

@end