//
//  main.m
//  ruby_message
//
//  Created by Graham Lee on 30/11/2013.
//  Copyright (c) 2013 Graham Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+RubyForward.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSObject *obj = [NSObject new];
        NSLog(@"%@ %@ %@ %@", [obj Hello: @"there"], [obj London], [obj tech], [obj talk]);
        [obj release];
    }
    return 0;
}
