#RubyMessage

This project replaces the Objective-C message-forwarding mechanism with something that looks a lot more like Ruby's `#method_missing`.

While everything in Ruby is an object, everything in Objective-C is _not_ an object. The design of the Objective-C runtime means that you cannot generally know the type of an argument given a message selector (more on this below), so this implementation guesses that everything is probably an object.

## More on that limitation

The Objective-C message-forwarding function must get its arguments from a variadic list, because the message itself can have any number of arguments. To safely grab an entry from a variadic list, you need to know the types of the arguments.

To know the type of an argument to an Objective-C method, you need to know the method signature. Unfortunately, inside the method-forwarding machinery you do not have a method signature so cannot know the types.

When you're using CoreFoundation's message-forwarding function, it requires you to implement `-methodSignatureForSelector:` so that it can pull the arguments out correctly to build the `NSInvocation` to pass to `-forwardInvocation:`. I didn't make you do that, so I can't know what the arguments should be.

## Licence

MIT. See COPYING.